# Tools for Software Development - Assignment

### Version 1.0

### Due date: Nov 15, 2020

This is a group assignment with 2 or 3 members in a team. You'll practice team based development using tools and topics covered in this course, including build tool (make), version control (git), issue tracking (via gitlab), documentation (markdown), and DevOps.

In this assignment, you can encouraged to utilise an integrated development tool (IDE) of your choice, such as VSCode, Eclipse, NetBeans, etc.

## Overview

For this assignment, you should apply for an account on GitLab.com, and create your own GitLab repository, with the initial files from 
          https://gitlab.com/ivanleeunisa/ande_assignment.git
Note: this code is broken and buggy.

## Instructions

1. Create milestones for your project. 

2. Create issues. 
	- Issues include bugs and features. 

3. Assign issues to team members. 
 	- That team member will then be responsible for:
		+ fixing the bug
		+ leaving a descriptive commit message
		+ pushing their changes
		+ closing this issue. 

4. Once all issues for a particular milestone have been fixed:
	- Create a tag for the milestone
	- Close the milestone
	
	Make sure this step is completed before starting on the issues for the next milestone.

	The first milestone will be related to fixing bugs in the supplied code. 
	The second milestone will be adding features from the supplied list. 
	The third milestone will be adding features you choose as a group.

5. Once milestone 3 has been completed, you will need to conduct a code review to improve and optimise the code.

6. Create a markdown file (should be added to the repository for version control):
	- manual.md:: detail how to use the software.
    
### Submission

The final submission is your git repository. 
	- **EACH** group member must make a submission. 
	- Each member needs to: 
		+ Clone your groups git repository
		+ Submit both your working directory *and* the '.git' folder. 


# Marking
This is a team-project to practice many of the team-development concepts and tools we cover in this course. 
It is important to note that team members do not have equal marks.
Individual contributions (such as your stored in the gitlab server) will be considered across all assessment criteria.

## Marking criteria

Programming

	- Fixing Bugs
	- Adding Features
	- New Features
	- These tasks require not just programming, but also git commits on feature branches.

Issue Tracking

	- Creating Issues
	- Closing Issues
	- Git commits that match the issue numbers

Git

	- Commit Log formatting
	- Commits, merges, tags, and branches

Misc

	- Creation of relevant files
	- Code review
	- Documentation Files

## Notes

It's important to note that: 

	- Individual marks according to the contributions recorded GitLab. 
	- Clean histories are important!
	- Portions of the marking (`Git`) are primarily _individual_, and the mark that you receive won't always match the rest of the group. 

# The Scenario
The supplied code is the pre-alpha version of a cash register program. 
Upon running the code, the user is prompted for their float (the amount of money in the cash register at the start of the day). 
The user then enters a product name, product price, and the amount of cash tendered by a customer. 
The program then prints to the screen the amount of change required by the customer and the balance of the cash in the cash register. 
As milestones are met the program will gain in functionality.

## Milestone 1: Fixing the Bugs (20%)
1. Clone your repository from gitlab
2. Download the starting code from the homepage
3. Add the starting code to your gitlab repository
4. Each team member will then need to clone the repository
5. Each team member then fixes the bugs assigned to them and uploads the changes 
6. Each team member must then close the issue assigned to them
7. Once all milestone 1 issues are resolved and the code has been checked to make sure then close
milestone 1 and tag the code as version1

### Summary of given bugs in this Milestone
1. Can't compile using the Makefile
2. Spelling errors in prompts
3. Change value is not correct
4. Ending balance of cash register is not displaying
5. No dollar signs before cash values

## Milestone 2: Adding Features (20%)
1. As before each user must add the features assigned to them, commit their code, and close the issue.
2. The group member tasked with translating the application's text prompts into another language will need to create a new branch, and add the translation on this branch (Use any web-based translation is fine). Once the translation is complete this branch will be tagged, and the group member will push their changes to the repository then return to the mainline without merging.
3. Once all milestone 2 issues have been closed, test the code, then tag it as version2 and close the milestone.

### List of activities in this Milestone
1. After entering the float present the user with a menu asking if they wish to exit, or process a transaction. Loop back to this menu after processing a transaction.
2. Remove the display of the cash register balance after each transaction and only display at the end when the user quits.
3. When processing a transaction allow multiple items to be bought at the same time by asking if all items have been entered after entering the item name and cost. If the user says no then another item is added to the transaction. If the user says yes the amount of cash tendered is requested and change calculated.
4. After processing a transaction present an option to provide a receipt. If the customer says yes print a receipt to the screen including the names and cost of all items bought, the total, the cash tendered, and the change.
5. Modify the Makefile to include creation of a jar file.
6. Add a nice welcome message once the program is started.
7. Add eloquent error handling in the case of unexpected input from the user.
8. Create a branch for an experimental version of the software in another language.

## Milestone 3: New Features (40%)
1. As a group you will need to choose new features to add functionality to your cash register program. Try to be original and unique as these features will form much of your softwares unique presentation and feel. Create these features as ‘issues’ and assign them to team members.
2. Each team member should be responsible on a new feature. All new features should be discussed among team members and should be interoperable as one project (as oppose to submitting independent incompatible code.)
3. One feature must be developed on a separate branch before being merged back into the mainline.
4. Once all features have been completed and closed, test the code, tag it as version3, then close milestone 3.

## User Manual (20%)
1. Create a user manual in markdown to instruct users how to download, install, and use your software. You should consider effective use of images in your user manual.
